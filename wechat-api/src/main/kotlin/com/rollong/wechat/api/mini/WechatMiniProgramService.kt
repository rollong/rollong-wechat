package com.rollong.wechat.api.mini

import cn.binarywang.wx.miniapp.api.WxMaJsapiService
import cn.binarywang.wx.miniapp.api.WxMaService
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl
import cn.binarywang.wx.miniapp.config.WxMaConfig
import com.rollong.wechat.token.manager.api.WechatTokenManageAPI
import com.rollong.wechat.token.manager.api.WechatTokenManagerException
import com.rollong.wechat.token.manager.api.request.GetAccessTokenRequest
import me.chanjar.weixin.common.error.WxRuntimeException
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 15:03
 * @project rollong-wechat
 * @filename WxMiniProgramService.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.api.mini
 * @description
 */
class WechatMiniProgramService(
    private val config: WechatMiniProgramConfig,
    private val tokenManageAPI: WechatTokenManageAPI
) : WxMaServiceImpl() {

    override fun getWxMaConfig(): WxMaConfig {
        return this.config
    }

    override fun addConfig(mpId: String?, configStorages: WxMaConfig?) {
        throw Exception("multi config not supported")
    }

    override fun switchover(mpId: String?): Boolean {
        throw Exception("multi config not supported")
    }

    override fun switchoverTo(miniappId: String?): WxMaService {
        throw Exception("multi config not supported")
    }

    private val jsapiService: WxMaJsapiService = WechatMiniProgramJsapiService(this, this.tokenManageAPI)

    override fun getAccessToken(forceRefresh: Boolean): String {
        if (!forceRefresh && !config.isAccessTokenExpired) {
            return this.config.accessToken!!
        }
        val lock = this.config.accessTokenLock
        var locked = false
        return try {
            do {
                locked = lock.tryLock(100, TimeUnit.MILLISECONDS)
                if (!forceRefresh && !this.config.isAccessTokenExpired) {
                    return this.config.accessToken!!
                }
            } while (!locked)
            val resp = this.tokenManageAPI.getAccessToken(
                request = GetAccessTokenRequest(
                    appId = this.config.appid, appSecret = this.config.secret,
                    type = this.config.config.type, note = this.config.config.note,
                    appName = this.config.config.appName,
                    refreshWhenTokenEquals = this.config.accessToken
                )
            )
            if (resp.code == 0) {
                this.config.updateAccessToken(resp.items?.token!!, resp.items?.expiresInSeconds!!)
            } else {
                throw WechatTokenManagerException(errCode = resp.errCode, message = resp.message)
            }
            this.config.accessToken!!
        } catch (e: IOException) {
            throw WxRuntimeException(e)
        } catch (e: InterruptedException) {
            throw WxRuntimeException(e)
        } finally {
            if (locked) {
                lock.unlock()
            }
        }
    }

    override fun extractAccessToken(resultContent: String?): String {
        throw RuntimeException("forbidden: not supposed to be here")
    }

    override fun getJsapiService(): WxMaJsapiService {
        return this.jsapiService
    }
}