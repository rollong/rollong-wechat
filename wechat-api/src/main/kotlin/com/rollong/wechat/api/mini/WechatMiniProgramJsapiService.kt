package com.rollong.wechat.api.mini

import cn.binarywang.wx.miniapp.api.WxMaService
import cn.binarywang.wx.miniapp.api.impl.WxMaJsapiServiceImpl
import com.rollong.wechat.token.manager.api.WechatTokenManageAPI
import com.rollong.wechat.token.manager.api.WechatTokenManagerException
import com.rollong.wechat.token.manager.api.request.GetTicketRequest

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 16:21
 * @project rollong-wechat
 * @filename WechatMiniProgramJsapiService.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.api.mini
 * @description
 */
class WechatMiniProgramJsapiService(
    private val maService: WxMaService,
    private val tokenManageAPI: WechatTokenManageAPI
) : WxMaJsapiServiceImpl(maService) {

    override fun getCardApiTicket(forceRefresh: Boolean): String {
        val config = this.maService.wxMaConfig
        if (forceRefresh) {
            config.expireCardApiTicket()
        }
        if (config.isCardApiTicketExpired) {
            val lock = this.maService.wxMaConfig.cardApiTicketLock
            lock.lock()
            try {
                if (config.isCardApiTicketExpired) {
                    val resp = this.tokenManageAPI.getTicket(
                        GetTicketRequest(
                            appId = config.appid,
                            appSecret = config.secret,
                            type = "wx_card",
                            refreshWhenTokenEquals = config.cardApiTicket
                        )
                    )
                    if (resp.code == 0) {
                        config.updateCardApiTicket(
                            resp.items?.token!!,
                            resp.items?.expiresInSeconds!!
                        )
                    } else {
                        throw WechatTokenManagerException(
                            errCode = resp.errCode,
                            message = resp.message
                        )
                    }
                }
            } finally {
                lock.unlock()
            }
        }
        return config.cardApiTicket
    }

    override fun getJsapiTicket(forceRefresh: Boolean): String {
        val config = this.maService.wxMaConfig
        if (forceRefresh) {
            config.expireJsapiTicket()
        }
        if (config.isJsapiTicketExpired) {
            val lock = this.maService.wxMaConfig.jsapiTicketLock
            lock.lock()
            try {
                if (config.isJsapiTicketExpired) {
                    val resp = this.tokenManageAPI.getTicket(
                        GetTicketRequest(
                            appId = config.appid,
                            appSecret = config.secret,
                            type = "jsapi",
                            refreshWhenTokenEquals = config.jsapiTicket
                        )
                    )
                    if (resp.code == 0) {
                        config.updateJsapiTicket(
                            resp.items?.token!!,
                            resp.items?.expiresInSeconds!!
                        )
                    } else {
                        throw WechatTokenManagerException(
                            errCode = resp.errCode,
                            message = resp.message
                        )
                    }
                }
            } finally {
                lock.unlock()
            }
        }
        return config.jsapiTicket
    }
}