package com.rollong.wechat.api.mini.helper

import cn.binarywang.wx.miniapp.api.WxMaService
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo

/**
 * @param code wx.login()后获取的code
 * @param userinfo 用户信息对象，不包含 openid 等敏感信息
 * @param encryptedUserinfo wx.getUserInfo()后获取的encryptedData,iv
 * @param encryptedPhone getPhoneNumber()后获取的encryptedData,iv
 */
fun WxMaService.jscode2Session(
    code: String,
    userInfo: WxMaUserInfo? = null,
    encryptedPhone: WxMaEncryptedData? = null,
    encryptedUserinfo: WxMaEncryptedData? = null
): WxMiniProgSession {
    val session = this.jsCode2SessionInfo(code)
    val decryptedPhoneNumber = if (null != encryptedPhone) {
        this.userService.getPhoneNoInfo(session.sessionKey, encryptedPhone.encryptedData, encryptedPhone.iv)
    } else {
        null
    }
    val decryptedUserInfo = if (null != encryptedUserinfo) {
        this.userService.getUserInfo(session.sessionKey, encryptedUserinfo.encryptedData, encryptedUserinfo.iv)
    } else {
        null
    }
    return WxMiniProgSession(
        openId = session.openid,
        unionId = session.unionid,
        sessionKey = session.sessionKey,
        mobile = decryptedPhoneNumber?.purePhoneNumber,
        mobileCountryCode = decryptedPhoneNumber?.countryCode,
        nickname = decryptedUserInfo?.nickName ?: (userInfo?.nickName),
        avatar = decryptedUserInfo?.avatarUrl ?: (userInfo?.avatarUrl),
        gender = when (decryptedUserInfo?.gender ?: (userInfo?.gender)) {
            "1" -> "男"
            "2" -> "女"
            null -> null
            else -> "未知"
        },
        country = decryptedUserInfo?.country ?: (userInfo?.country),
        province = decryptedUserInfo?.province ?: (userInfo?.province),
        city = decryptedUserInfo?.city ?: (userInfo?.city),
        language = decryptedUserInfo?.language ?: (userInfo?.language)
    )
}

data class WxMaEncryptedData(
    var encryptedData: String? = null,
    var iv: String? = null
)

data class WxMiniProgSession(
    var openId: String? = null,
    var unionId: String? = null,
    var sessionKey: String? = null,
    var mobile: String? = null,
    var mobileCountryCode: String? = null,
    var nickname: String? = null,
    var avatar: String? = null,
    var gender: String? = null,
    var country: String? = null,
    var province: String? = null,
    var city: String? = null,
    var language: String? = null
)