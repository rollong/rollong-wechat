package com.rollong.wechat.api.token

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 13:42
 * @project rollong-wechat
 * @filename Token.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.api.token
 * @description 字符串token以及lock/expireTime实现
 */
class Token {

    val lock: Lock = ReentrantLock()
    var token: String? = null
    var expiresAt: Long = 0L

    fun update(token: String, expiresInSeconds: Int) {
        this.token = token
        this.expiresAt = System.currentTimeMillis() + (expiresInSeconds - 2) * 1000L
    }

    fun expired() = System.currentTimeMillis() >= this.expiresAt - 10000L

    fun setExpired() {
        this.token = null
        this.expiresAt = -1
    }
}