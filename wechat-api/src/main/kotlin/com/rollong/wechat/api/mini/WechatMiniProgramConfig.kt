package com.rollong.wechat.api.mini

import cn.binarywang.wx.miniapp.config.WxMaConfig
import com.rollong.wechat.api.token.Token
import com.rollong.wechat.common.WechatAppConfig
import me.chanjar.weixin.common.bean.WxAccessToken
import java.util.concurrent.locks.Lock

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 15:39
 * @project rollong-wechat
 * @filename WechatMiniProgramConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.api.mini
 * @description
 */
class WechatMiniProgramConfig(
    val config: WechatAppConfig
) : WxMaConfig {

    private val accessToken: Token = Token()
    private val jsApiToken: Token = Token()
    private val cardApiToken: Token = Token()
    private var apiHostUrl: String = ""

    override fun getAccessToken(): String? {
        return this.accessToken.token
    }

    override fun getAccessTokenLock(): Lock {
        return this.accessToken.lock
    }

    override fun getExpiresTime(): Long {
        return this.accessToken.expiresAt
    }

    override fun isAccessTokenExpired(): Boolean {
        return this.accessToken.expired()
    }

    override fun expireAccessToken() {
        this.accessToken.setExpired()
    }

    override fun getJsapiTicket(): String? {
        return this.jsApiToken.token
    }

    override fun getJsapiTicketLock(): Lock {
        return this.jsApiToken.lock
    }

    override fun isJsapiTicketExpired(): Boolean {
        return this.jsApiToken.expired()
    }

    override fun expireJsapiTicket() {
        this.jsApiToken.setExpired()
    }

    override fun getCardApiTicket(): String? {
        return this.cardApiToken.token
    }

    override fun getCardApiTicketLock(): Lock {
        return this.cardApiToken.lock
    }

    override fun isCardApiTicketExpired(): Boolean {
        return this.cardApiToken.expired()
    }

    override fun expireCardApiTicket() {
        this.cardApiToken.setExpired()
    }

    override fun getAppid(): String {
        return this.config.appId
    }

    override fun getSecret(): String {
        return this.config.appSecret
    }

    override fun getToken(): String? {
        return this.config.token
    }

    override fun getAesKey(): String? {
        return this.config.aesKey
    }

    override fun getMsgDataFormat(): String? {
        return this.config.msgDataFormat
    }

    override fun getOriginalId(): String? {
        return this.config.originalId
    }

    override fun getCloudEnv(): String? {
        return this.config.cloudEnv
    }

    override fun autoRefreshToken(): Boolean {
        return false
    }

    override fun setApiHostUrl(apiHostUrl: String?) {
        this.apiHostUrl = apiHostUrl ?: ""
    }

    override fun getApiHostUrl(): String {
        return this.apiHostUrl
    }

    override fun getApacheHttpClientBuilder() = null

    override fun getHttpProxyHost() = null

    override fun getHttpProxyPassword() = null

    override fun getHttpProxyPort() = 8080

    override fun getHttpProxyUsername() = null

    override fun updateAccessToken(accessToken: WxAccessToken) {
        this.updateAccessToken(accessToken.accessToken, accessToken.expiresIn)
    }

    override fun updateAccessToken(accessToken: String, expiresInSeconds: Int) {
        this.accessToken.update(accessToken, expiresInSeconds)
    }

    override fun updateCardApiTicket(apiTicket: String, expiresInSeconds: Int) {
        this.cardApiToken.update(apiTicket, expiresInSeconds)
    }

    override fun updateJsapiTicket(jsapiTicket: String, expiresInSeconds: Int) {
        this.jsApiToken.update(jsapiTicket, expiresInSeconds)
    }

}