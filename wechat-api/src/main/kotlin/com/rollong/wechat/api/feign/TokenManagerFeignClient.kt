package com.rollong.wechat.api.feign

import com.rollong.wechat.token.manager.api.WechatTokenManageAPI
import org.springframework.cloud.openfeign.FeignClient

@FeignClient(name = "rollong-wechat", url = "https://api.wx.rollong.com")
interface TokenManagerFeignClient : WechatTokenManageAPI