package com.rollong.wechat.common

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 15:08
 * @project rollong-wechat
 * @filename WechatAppType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.common
 * @description
 */
enum class WechatAppType {
    Popular, MiniProgram
}