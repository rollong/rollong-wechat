package com.rollong.wechat.common

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/09 22:31
 * @project rollong-wechat
 * @filename WechatErrorCode.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.common
 * @description
 */
/**
 * -1	系统繁忙，此时请开发者稍候再试
 * 0	请求成功
 * 40001	AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性
 * 40002	请确保 grant_type 字段值为 client_credential
 * 40013	不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
 */
enum class WechatErrorCode(
    val code: Int
) {
    SysBusy(-1), Success(0), AppSecret(40001), GrantType(40002), AppId(40013)
}