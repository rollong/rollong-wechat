package com.rollong.wechat.common

class WechatAppConfig(
    var appId: String = "",
    var appSecret: String = "",
    var type: WechatAppType = WechatAppType.MiniProgram,
    var token: String? = null,
    var aesKey: String? = null,
    var appName: String? = null,
    var note: String? = null,
    //消息格式，XML或者JSON
    var msgDataFormat: String? = null,
    var cloudEnv: String? = null,
    var originalId: String? = null
)