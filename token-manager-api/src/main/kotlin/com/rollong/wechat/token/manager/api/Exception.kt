package com.rollong.wechat.token.manager.api

import com.rollong.common.exception.BaseException

object ExceptionCode {
    const val OK = "ok"
    const val FAIL = "failed"
    const val APP_ID_NOT_FOUND = "app_id_not_found"
    const val APP_SECRET_NOT_RIGHT = "app_secret_not_right"
    const val WECHAT_API_FAILED = "wechat_api_failed"
    const val BAD_REQUEST = "bad_request"
    const val NO_ACCESS_TOKEN = "no_access_token"
}

open class WechatTokenManagerException(
    errCode: String = ExceptionCode.FAIL,
    override var message: String? = null,
    override var cause: Throwable? = null
) : BaseException(
    errCode = errCode,
    message = message,
    httpStatus = 200
)

class AppIdNotFound(
    appId: String
) : WechatTokenManagerException(
    errCode = ExceptionCode.APP_ID_NOT_FOUND,
    message = "appId=${appId}未找到"
)

class AppSecretWrong(
    appId: String
) : WechatTokenManagerException(
    errCode = ExceptionCode.APP_SECRET_NOT_RIGHT,
    message = "appId=${appId} ,secret不正确"
)

class BadRequest(
    message: String
) : WechatTokenManagerException(
    errCode = ExceptionCode.BAD_REQUEST,
    message = message
)

class NoAccessToken : WechatTokenManagerException(
    errCode = ExceptionCode.NO_ACCESS_TOKEN,
    message = "没有获取到access_token"
)