package com.rollong.wechat.token.manager.api.response

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 20:38
 * @project rollong-wechat
 * @filename TokenItem.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.api.response
 * @description
 */
data class TokenItem(
    var token: String? = null,
    var expiresAt: Long? = null,
    var expiresInSeconds: Int? = null
)