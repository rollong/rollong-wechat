package com.rollong.wechat.token.manager.api.request

import com.rollong.wechat.common.WechatAppType

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 15:07
 * @project rollong-wechat
 * @filename GetAccessTokenRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.api.request
 * @description
 */
data class GetAccessTokenRequest(
    var appId: String = "",
    var appSecret: String = "",
    var type: WechatAppType? = null,
    var note: String? = null,
    var appName: String? = null,
    var forceRefresh: Boolean? = null,
    var refreshWhenTokenEquals: String? = null
)