package com.rollong.wechat.token.manager.api.response

import com.rollong.wechat.token.manager.api.ExceptionCode

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 15:09
 * @project rollong-wechat
 * @filename Token.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.api.response
 * @description
 */
data class WechatTokenResponse(
    var errCode: String = ExceptionCode.OK,
    var code: Int = 0,
    var message: String = "",
    var items: TokenItem? = null
) {
    constructor(
        token: String,
        expiresAt: Long,
        expiresInSeconds: Int
    ) : this(
        items = TokenItem(
            token = token, expiresAt = expiresAt, expiresInSeconds = expiresInSeconds
        )
    )
}