package com.rollong.wechat.token.manager.api

import com.rollong.wechat.token.manager.api.request.GetAccessTokenRequest
import com.rollong.wechat.token.manager.api.request.GetTicketRequest
import com.rollong.wechat.token.manager.api.response.WechatTokenResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping(ApiConstant.WX)
interface WechatTokenManageAPI {

    @PostMapping("getAccessToken")
    fun getAccessToken(
        @RequestBody request: GetAccessTokenRequest
    ): WechatTokenResponse

    @PostMapping("getTicket")
    fun getTicket(
        @RequestBody request: GetTicketRequest
    ): WechatTokenResponse
}