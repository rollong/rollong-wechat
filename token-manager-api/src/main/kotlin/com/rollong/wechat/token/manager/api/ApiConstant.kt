package com.rollong.wechat.token.manager.api

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 14:59
 * @project rollong-wechat
 * @filename ApiConstant.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.api
 * @description
 */
object ApiConstant {
    const val BASE_URL = "/api/v1"
    const val WX = "$BASE_URL/wx/"
}