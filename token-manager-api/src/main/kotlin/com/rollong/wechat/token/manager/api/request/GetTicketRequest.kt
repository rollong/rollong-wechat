package com.rollong.wechat.token.manager.api.request

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 15:08
 * @project rollong-wechat
 * @filename GetTicketRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.api.request
 * @description
 */
data class GetTicketRequest(
    var appId: String = "",
    var appSecret: String = "",
    var type: String = "",
    var forceRefresh: Boolean = false,
    var refreshWhenTokenEquals: String? = null
)