1. 项目依赖

[WxJava](https://github.com/Wechat-Group/WxJava)

[WxJava@OSC mirror](https://gitee.com/binary/weixin-java-tools)

```
dependencies {
      implementation 'org.bitbucket.rollong.rollong-wechat:wechat-api:${rollongWechatVersion}'
}
```
