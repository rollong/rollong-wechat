#!/usr/bin/env bash
docker run --rm -d -p 5432:5432 \
  -e POSTGRES_USER=homestead -e POSTGRES_DB=rollong-wxapps -e POSTGRES_PASSWORD=secret \
  postgres:13