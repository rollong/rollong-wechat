package com.rollong.wechat.token.manager.server

import com.rollong.wechat.common.WechatAppType
import com.rollong.wechat.token.manager.api.request.GetAccessTokenRequest
import com.rollong.wechat.token.manager.server.service.WxTokenService
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/09 22:38
 * @project rollong-wechat
 * @filename ServerTest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner::class)
class ServerTest {

    @Autowired
    private lateinit var tokenService: WxTokenService

    @Test
    fun accessToken() {
        val resp = this.tokenService.createOrUpdateApp(
            request = GetAccessTokenRequest(
                appId = "123",
                appSecret = "123",
                type = WechatAppType.Popular
            )
        )
        println(resp)
    }
}