package com.rollong.wechat.token.manager.server.openfeign

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/09 22:29
 * @project rollong-wechat
 * @filename TicketResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.openfeign
 * @description
 */
data class TicketResponse(
    @JsonProperty("ticket")
    var ticket: String = "",
    @JsonProperty("expires_in")
    var expiresIn: Long = 0L,
    @JsonProperty("errcode")
    var errCode: Int? = null,
    @JsonProperty("errmsg")
    var errMsg: String? = null
)