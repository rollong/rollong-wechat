package com.rollong.wechat.token.manager.server.persistence.entity

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 17:52
 * @project 微信token-daemon
 * @filename WxTicketPK.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.persistence.entity
 * @description  微信ticket主键
 */
@Embeddable
data class WxTicketPK(
    @Column(name = "app_id")
    var appId: String = "",
    @Column(name = "type")
    var type: String = ""
) : Serializable