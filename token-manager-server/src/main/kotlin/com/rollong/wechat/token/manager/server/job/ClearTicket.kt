package com.rollong.wechat.token.manager.server.job

import com.alibaba.schedulerx.worker.domain.JobContext
import com.alibaba.schedulerx.worker.processor.JavaProcessor
import com.alibaba.schedulerx.worker.processor.ProcessResult
import com.rollong.wechat.token.manager.server.persistence.repository.WxTicketRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-29 15:09
 * @project rollong-wechat
 * @filename ClearTicket.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.processor
 * @description 定期清理tickets
 */
@Component
class ClearTicket(
    private val ticketRepository: WxTicketRepository
) : JavaProcessor() {

    companion object {
        private val logger = LoggerFactory.getLogger(ClearTicket::class.java)
    }

    @Transactional
    override fun process(context: JobContext?): ProcessResult {
        try {
            this.ticketRepository.deleteAllExpiring(before = Timestamp(System.currentTimeMillis() + 10 * 60 * 1000L))
            return ProcessResult(true)
        } catch (e: Throwable) {
            logger.error("ClearTicketJob failed", e)
            throw e
        }
    }
}