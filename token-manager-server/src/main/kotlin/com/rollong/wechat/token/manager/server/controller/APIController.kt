package com.rollong.wechat.token.manager.server.controller

import com.rollong.common.exception.BaseException
import com.rollong.wechat.token.manager.api.ExceptionCode
import com.rollong.wechat.token.manager.api.response.WechatTokenResponse
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-28 0:57
 * @project rollong-wechat
 * @filename APIController.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.controller
 * @description
 */
@Component
abstract class APIController {

    @ExceptionHandler(Throwable::class)
    fun exceptionHandler(
        e: Throwable,
        request: HttpServletRequest,
        response: HttpServletResponse
    ): WechatTokenResponse {
        return when (e) {
            is BaseException -> {
                response.status = HttpStatus.OK.value()
                WechatTokenResponse(
                    code = e.code,
                    errCode = e.errCode,
                    message = e.message ?: e::class.java.name
                )
            }
            is MethodArgumentNotValidException -> {
                response.status = HttpStatus.BAD_REQUEST.value()
                WechatTokenResponse(
                    code = 422,
                    errCode = ExceptionCode.BAD_REQUEST,
                    message = "请求不合法:${e.message}"
                )
            }
            else -> {
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                WechatTokenResponse(
                    code = 500,
                    errCode = ExceptionCode.FAIL,
                    message = e.message ?: e::class.java.name
                )
            }
        }
    }
}