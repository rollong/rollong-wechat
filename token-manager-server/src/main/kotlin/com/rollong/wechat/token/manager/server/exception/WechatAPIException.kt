package com.rollong.wechat.token.manager.server.exception

import com.rollong.common.exception.APICallException
import com.rollong.wechat.token.manager.api.ExceptionCode

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-27 17:05
 * @project rollong-wechat
 * @filename WechatAPIException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.exception
 * @description 调用微信接口返回的错误
 */
class WechatAPIException(
    wxErrCode: Int? = null,
    wxErrMsg: String? = null
) : APICallException(
    errCode = ExceptionCode.WECHAT_API_FAILED,
    serverErrCode = wxErrCode?.toString(),
    serverErrMessage = wxErrMsg,
    message = "微信服务报错:${wxErrCode}:${wxErrMsg}"
)