package com.rollong.wechat.token.manager.server.persistence.repository

import com.rollong.wechat.token.manager.server.persistence.entity.WxTicket
import com.rollong.wechat.token.manager.server.persistence.entity.WxTicketPK
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 17:57
 * @project 微信token-daemon
 * @filename WxTicketRepository.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.persistence.repository
 * @description
 */

@Repository
interface WxTicketRepository : CrudRepository<WxTicket, WxTicketPK> {
    @Query("from WxTicket as t where t.expiresAt <= :before")
    fun findAllExpiring(@Param("before") before: Timestamp): List<WxTicket>

    @Modifying
    @Query("delete from WxTicket as t where t.expiresAt <= :before")
    fun deleteAllExpiring(@Param("before") before: Timestamp)
}