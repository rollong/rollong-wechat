package com.rollong.wechat.token.manager.server.job

import com.alibaba.schedulerx.worker.domain.JobContext
import com.alibaba.schedulerx.worker.processor.JavaProcessor
import com.alibaba.schedulerx.worker.processor.ProcessResult
import com.rollong.wechat.token.manager.server.openfeign.WechatAPI
import com.rollong.wechat.token.manager.server.persistence.repository.WxAppRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-29 14:51
 * @project rollong-wechat
 * @filename CheckAccessToken.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.processor
 * @description 检查AccessToken的任务
 */
@Component
class CheckAccessToken(
    private val wechatAPI: WechatAPI,
    private val wxAppRepository: WxAppRepository
) : JavaProcessor() {

    companion object {
        private val logger = LoggerFactory.getLogger(CheckAccessToken::class.java)
    }

    @Transactional
    override fun process(context: JobContext?): ProcessResult {
        this.wxAppRepository.findAllNotExpiring(after = Timestamp(System.currentTimeMillis() + 10 * 60 * 1000L)).forEach {
            try {
                val accessToken = it.accessToken!!
                val resp = this.wechatAPI.callbackIP(accessToken = accessToken)
                if (resp.errCode != null) {
                    logger.error("${it.id}的access_token失效,返回${resp.errCode}/${resp.errMsg}")
                    it.accessToken = null
                    it.accessTokenExpiresAt = null
                    this.wxAppRepository.save(it)
                }
                it.accessToken = null
            } catch (e: Throwable) {
                logger.error("TokenScheduler::checkAccessToken failed", e)
                throw e
            }
        }
        return ProcessResult(true)
    }
}