package com.rollong.wechat.token.manager.server.persistence.repository

import com.rollong.wechat.token.manager.server.persistence.entity.WxApp
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 17:23
 * @project 微信token-daemon
 * @filename WxAppRepository.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.persistence.repository
 * @description
 */

@Repository
interface WxAppRepository : CrudRepository<WxApp, String> {

    @Modifying
    @Query(
        "update WxApp as wx set wx.accessToken = null, wx.accessTokenExpiresAt = null " +
            "where wx.accessToken is not null and (wx.accessTokenExpiresAt is null or wx.accessTokenExpiresAt <= :before)"
    )
    fun clearAllExpiring(@Param("before") before: Timestamp)

    @Query("from WxApp as wx where wx.enabled = true and wx.accessToken is not null and wx.accessTokenExpiresAt is not null and wx.accessTokenExpiresAt > :after")
    fun findAllNotExpiring(@Param("after") after: Timestamp): List<WxApp>
}