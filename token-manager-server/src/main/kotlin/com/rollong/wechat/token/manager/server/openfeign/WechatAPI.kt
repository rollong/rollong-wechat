package com.rollong.wechat.token.manager.server.openfeign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/09 22:01
 * @project rollong-wechat
 * @filename WechatAPI.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.openfeign
 * @description
 */
@FeignClient(name = "api.weixin.qq.com", url = "https://api.weixin.qq.com/")
interface WechatAPI {

    @GetMapping("cgi-bin/token")
    fun accessToken(
        @RequestParam(name = "grant_type", required = true) grantType: String = "client_credential",
        @RequestParam(name = "appid", required = true) appid: String,
        @RequestParam(name = "secret", required = true) secret: String
    ): TokenResponse

    @GetMapping("cgi-bin/ticket/getticket")
    fun ticket(
        @RequestParam(name = "access_token", required = true) accessToken: String,
        @RequestParam(name = "type", required = true) type: String
    ): TicketResponse

    @GetMapping("cgi-bin/getcallbackip")
    fun callbackIP(
        @RequestParam(name = "access_token", required = true) accessToken: String,
    ): CallbackIP
}