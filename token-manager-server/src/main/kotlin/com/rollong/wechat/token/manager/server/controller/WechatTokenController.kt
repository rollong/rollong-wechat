package com.rollong.wechat.token.manager.server.controller

import com.rollong.wechat.token.manager.api.WechatTokenManageAPI
import com.rollong.wechat.token.manager.api.request.GetAccessTokenRequest
import com.rollong.wechat.token.manager.api.request.GetTicketRequest
import com.rollong.wechat.token.manager.api.response.WechatTokenResponse
import com.rollong.wechat.token.manager.server.service.WxTokenService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
@Api(description = "后台管理", tags = ["后台管理"])
class WechatTokenController(
    private val tokenService: WxTokenService
) : WechatTokenManageAPI, APIController() {

    @ApiOperation(value = "获取accessToken")
    @Transactional
    override fun getAccessToken(
        @RequestBody request: GetAccessTokenRequest
    ): WechatTokenResponse {
        return this.tokenService.getAccessToken(
            request = request
        )
    }

    @ApiOperation(value = "获取ticket")
    @Transactional
    override fun getTicket(
        @RequestBody request: GetTicketRequest
    ): WechatTokenResponse {
        return this.tokenService.getTicket(
            request = request
        )
    }
}