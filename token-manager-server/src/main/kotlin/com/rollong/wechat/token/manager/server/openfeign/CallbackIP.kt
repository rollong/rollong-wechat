package com.rollong.wechat.token.manager.server.openfeign

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-29 15:00
 * @project rollong-wechat
 * @filename CallbackIP.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.service
 * @description
 */
data class CallbackIP(
    var errCode: String? = null,
    var errMsg: String? = null
)