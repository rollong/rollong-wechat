package com.rollong.wechat.token.manager.server.persistence.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.rollong.common.sql.entity.AbstractEntity
import com.rollong.common.sql.entity.HasCreateTime
import com.rollong.common.sql.entity.HasUpdateTime
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "apps", schema = "s_wechat")
class WxApp : AbstractEntity<String>(), HasCreateTime, HasUpdateTime {

    @Id
    @Column(name = "app_id", nullable = false)
    override var id: String? = null

    @Basic
    @Column(name = "app_secret")
    var appSecret: String = ""

    @Basic
    @Column(name = "app_name")
    var appName: String? = null

    @Basic
    @Column(name = "type")
    var type: String = ""

    @Basic
    @Column(name = "access_token")
    @get:JsonIgnore
    var accessToken: String? = null

    @Basic
    @Column(name = "access_token_expires_at")
    @get:JsonIgnore
    var accessTokenExpiresAt: Timestamp? = null

    @Basic
    @Column(name = "enabled")
    var enabled: Boolean = true

    @Basic
    @Column(name = "note")
    var note: String? = null

    @Basic
    @Column(name = "created_at", nullable = false)
    @get:JsonIgnore
    override var createdAt: Timestamp = Timestamp(0)

    @Basic
    @Column(name = "updated_at", nullable = false)
    @get:JsonIgnore
    override var updatedAt: Timestamp = Timestamp(0)
}