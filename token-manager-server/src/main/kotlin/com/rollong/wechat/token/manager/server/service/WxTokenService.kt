package com.rollong.wechat.token.manager.server.service

import com.rollong.common.sql.entity.created
import com.rollong.wechat.token.manager.api.AppIdNotFound
import com.rollong.wechat.token.manager.api.AppSecretWrong
import com.rollong.wechat.token.manager.api.BadRequest
import com.rollong.wechat.token.manager.api.request.GetAccessTokenRequest
import com.rollong.wechat.token.manager.api.request.GetTicketRequest
import com.rollong.wechat.token.manager.api.response.WechatTokenResponse
import com.rollong.wechat.token.manager.server.persistence.entity.WxApp
import com.rollong.wechat.token.manager.server.persistence.entity.WxTicketPK
import com.rollong.wechat.token.manager.server.persistence.repository.WxAppRepository
import com.rollong.wechat.token.manager.server.persistence.repository.WxTicketRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 17:27
 * @project 微信token-daemon
 * @filename WxTokenService.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.service
 * @description  微信token服务
 */

@Service
class WxTokenService(
    private val repository: WxAppRepository,
    private val ticketRepository: WxTicketRepository,
    private val tokenRefresher: TokenRefresher
) {

    private val logger = LoggerFactory.getLogger(WxTokenService::class.java)

    private fun getApp(
        appId: String,
        appSecret: String
    ): WxApp {
        val app = this.repository.findById(appId).orElseThrow { AppIdNotFound(appId) }
        if (app.appSecret != appSecret) {
            throw AppSecretWrong(appId)
        }
        return app
    }

    @Transactional
    fun createOrUpdateApp(
        request: GetAccessTokenRequest
    ): WxApp {
        var save = false
        var newToken = false
        var app = this.repository.findById(request.appId).orElseGet {
            save = true
            newToken = true
            val a = WxApp()
            a.created()
            a.id = request.appId
            a.appSecret = request.appSecret
            a.type = request.type?.name ?: throw BadRequest("type必填")
            a
        }
        if (app.appSecret != request.appSecret) {
            newToken = true
            save = true
            app.appSecret = request.appSecret
        }
        request.type?.let { if (app.type != it.name) save = true; app.type = it.name }
        request.note?.let { if (app.note != it) save = true;app.note = it }
        request.appName?.let { if (app.appName != it) save = true;app.appName = it }
        app = if (save) {
            this.repository.save(app)
        } else {
            app
        }
        return if (newToken || true == request.forceRefresh) {
            this.tokenRefresher.accessToken(app = app)
        } else {
            app
        }
    }

    @Transactional
    fun getAccessToken(request: GetAccessTokenRequest): WechatTokenResponse {
        try {
            val app = this.createOrUpdateApp(request = request)
            return if (null != app.accessToken &&
                (app.accessTokenExpiresAt?.time ?: 0L) >= System.currentTimeMillis() - 60 * 1000L &&
                app.accessToken != request.refreshWhenTokenEquals
            ) {
                WechatTokenResponse(
                    token = app.accessToken!!,
                    expiresAt = app.accessTokenExpiresAt?.time!!,
                    expiresInSeconds = ((app.accessTokenExpiresAt?.time!! - System.currentTimeMillis()) / 1000L).toInt()
                )
            } else {
                this.tokenRefresher.accessToken(app = app).let {
                    WechatTokenResponse(
                        token = it.accessToken!!,
                        expiresAt = it.accessTokenExpiresAt?.time!!,
                        expiresInSeconds = ((it.accessTokenExpiresAt?.time!! - System.currentTimeMillis()) / 1000L).toInt()
                    )
                }
            }
        } catch (e: Exception) {
            this.logger.error("ACCESS_TOKEN refurbish error with appid:{}", request.appId)
            this.logger.error("", e)
            throw e
        }
    }

    /**
     * @author kwang on 2019/8/26 18:03
     * @param appId appId
     * @param type wx_card or jsapi
     * @param appSecret appSecret
     * @return
     * @description
     */
    @Transactional(readOnly = true)
    fun getTicket(
        request: GetTicketRequest
    ): WechatTokenResponse {
        try {
            this.getApp(request.appId, request.appSecret)
            val pk = WxTicketPK(appId = request.appId, type = request.type)
            val wxTicket = this.ticketRepository.findById(pk).orElseGet { null }
            return if (null != wxTicket && wxTicket.ticket != "" &&
                (wxTicket.expiresAt.time) >= System.currentTimeMillis() - 20 * 1000L &&
                !request.forceRefresh &&
                wxTicket.ticket != request.refreshWhenTokenEquals
            ) {
                WechatTokenResponse(
                    token = wxTicket.ticket,
                    expiresAt = wxTicket.expiresAt.time,
                    expiresInSeconds = ((wxTicket.expiresAt.time - System.currentTimeMillis()) / 1000L).toInt()
                )
            } else {
                this.tokenRefresher.jsTicket(pk = pk).get().let {
                    WechatTokenResponse(
                        token = it.ticket,
                        expiresAt = it.expiresAt.time,
                        expiresInSeconds = ((it.expiresAt.time - System.currentTimeMillis()) / 1000L).toInt()
                    )
                }
            }
        } catch (e: Exception) {
            this.logger.error("TICKET refurbish error with appid:{} type:{}", request.appId, request.type)
            this.logger.error("", e)
            throw e
        }
    }
}