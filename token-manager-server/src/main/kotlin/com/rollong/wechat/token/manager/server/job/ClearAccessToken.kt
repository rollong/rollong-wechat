package com.rollong.wechat.token.manager.server.job

import com.alibaba.schedulerx.worker.domain.JobContext
import com.alibaba.schedulerx.worker.processor.JavaProcessor
import com.alibaba.schedulerx.worker.processor.ProcessResult
import com.rollong.wechat.token.manager.server.persistence.repository.WxAppRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-29 15:07
 * @project rollong-wechat
 * @filename ClearAccessToken.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.processor
 * @description 定期清理access_token
 */
@Component
class ClearAccessToken(
    private val wxAppRepository: WxAppRepository
) : JavaProcessor() {

    companion object {
        private val logger = LoggerFactory.getLogger(ClearAccessToken::class.java)
    }

    @Transactional
    override fun process(context: JobContext?): ProcessResult {
        try {
            this.wxAppRepository.clearAllExpiring(before = Timestamp(System.currentTimeMillis() + 10 * 60 * 1000L))
            return ProcessResult(true)
        } catch (e: Throwable) {
            logger.error("ClearAccessTokenJob failed", e)
            throw e
        }
    }
}