package com.rollong.wechat.token.manager.server.service

import com.rollong.common.sql.entity.created
import com.rollong.wechat.common.WechatErrorCode
import com.rollong.wechat.token.manager.api.AppIdNotFound
import com.rollong.wechat.token.manager.api.NoAccessToken
import com.rollong.wechat.token.manager.server.exception.WechatAPIException
import com.rollong.wechat.token.manager.server.openfeign.WechatAPI
import com.rollong.wechat.token.manager.server.persistence.entity.WxApp
import com.rollong.wechat.token.manager.server.persistence.entity.WxTicket
import com.rollong.wechat.token.manager.server.persistence.entity.WxTicketPK
import com.rollong.wechat.token.manager.server.persistence.repository.WxAppRepository
import com.rollong.wechat.token.manager.server.persistence.repository.WxTicketRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import java.util.concurrent.CompletableFuture

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 18:45
 * @project 微信token-daemon
 * @filename TokenRefresher.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.service
 * @description
 */
@Service
class TokenRefresher(
    private val wechatAPI: WechatAPI,
    private val wxAppRepository: WxAppRepository,
    private val ticketRepository: WxTicketRepository
) {

    private val logger = LoggerFactory.getLogger(TokenRefresher::class.java)

    @Transactional
    fun accessToken(app: WxApp): WxApp {
        try {
            val token = this.wechatAPI.accessToken(appid = app.id!!, secret = app.appSecret)
            if (
                token.accessToken.isEmpty() &&
                token.errCode != null &&
                WechatErrorCode.Success.code != token.errCode
            ) {
                throw WechatAPIException(wxErrCode = token.errCode, wxErrMsg = token.errMsg)
            }
            app.accessToken = token.accessToken
            app.accessTokenExpiresAt = Timestamp(System.currentTimeMillis() + token.expiresIn * 1000L)
            this.logger.info("ACCESS_TOKEN refurbish with appid:{}", app.id)
            return this.wxAppRepository.save(app)
        } catch (e: Exception) {
            this.logger.error("ACCESS_TOKEN refurbish error with appid:{}", app.id)
            this.logger.error("", e)
            throw e
        }
    }

    @Transactional
    @Async
    fun jsTicket(pk: WxTicketPK): CompletableFuture<WxTicket> {
        try {
            val app = this.wxAppRepository.findById(pk.appId)
                .orElseThrow { AppIdNotFound(pk.appId) }
            val wxTicket = this.ticketRepository.findById(pk).orElseGet {
                val t = WxTicket()
                t.created()
                t.id = pk
                t
            }
            val accessToken = app.accessToken ?: (this.accessToken(app).accessToken ?: throw NoAccessToken())
            val ticket = this.wechatAPI.ticket(accessToken, pk.type)
            if (
                ticket.errCode != null &&
                WechatErrorCode.Success.code != ticket.errCode
            ) {
                throw WechatAPIException(wxErrCode = ticket.errCode, wxErrMsg = ticket.errMsg)
            }
            wxTicket.ticket = ticket.ticket
            wxTicket.expiresAt = Timestamp(System.currentTimeMillis() + (ticket.expiresIn - 60) * 1000L)
            this.logger.info("TICKET refurbish with appid:{} type:{}", pk.appId, pk.type)
            return CompletableFuture.completedFuture(this.ticketRepository.save(wxTicket))
        } catch (e: Exception) {
            this.logger.error("TICKET refurbish error with appid:{} type:{}", pk.appId, pk.type)
            this.logger.error("", e)
            throw e
        }
    }
}