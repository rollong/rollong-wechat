package com.rollong.wechat.token.manager.server.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Value("\${swagger.enable}")
    private var swaggerEnabled: Boolean = false

    @Bean(value = ["defaultApi"])
    fun defaultApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .enable(swaggerEnabled)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.rollong.wechat.token.manager.server"))
            .paths(PathSelectors.any())
            .build()
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
            .title("Rollong-notify RESTful APIs")
            .contact(Contact("Rollong", "rollong.com", ""))
            .description("Rollong-notify RESTful APIs")
            .termsOfServiceUrl("https://www.rollong.com/")
            .version("1.0")
            .build()
    }
}