package com.rollong.wechat.token.manager.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
@ComponentScan(basePackages = ["com.rollong"])
@EnableFeignClients(value = ["com.rollong.wechat.token.manager.server.openfeign"])
class WxTokenDaemonApplication

fun main(args: Array<String>) {
    runApplication<WxTokenDaemonApplication>(*args)
}
