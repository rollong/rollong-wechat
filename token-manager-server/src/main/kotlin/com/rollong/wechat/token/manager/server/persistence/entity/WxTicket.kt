package com.rollong.wechat.token.manager.server.persistence.entity

import com.rollong.common.sql.entity.AbstractEntity
import com.rollong.common.sql.entity.HasCreateTime
import com.rollong.common.sql.entity.HasUpdateTime
import java.sql.Timestamp
import javax.persistence.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/26 17:50
 * @project 微信token-daemon
 * @filename WxTicket.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.persistence.entity
 * @description https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=wx_card
 */

@Entity
@Table(name = "wx_ticket", schema = "s_wechat")
class WxTicket : AbstractEntity<WxTicketPK>(), HasCreateTime, HasUpdateTime {

    @EmbeddedId
    override var id: WxTicketPK? = null

    @Basic
    @Column(name = "ticket")
    var ticket: String = ""

    @Basic
    @Column(name = "expires_at")
    var expiresAt: Timestamp = Timestamp(0)

    @Basic
    @Column(name = "created_at", nullable = false)
    override var createdAt: Timestamp = Timestamp(0)

    @Basic
    @Column(name = "updated_at", nullable = false)
    override var updatedAt: Timestamp = Timestamp(0)
}