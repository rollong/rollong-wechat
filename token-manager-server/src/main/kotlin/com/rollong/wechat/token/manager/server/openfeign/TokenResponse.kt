package com.rollong.wechat.token.manager.server.openfeign

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/09 22:09
 * @project rollong-wechat
 * @filename TokenResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.wechat.token.manager.server.openfeign
 * @description
 */
data class TokenResponse(
    @JsonProperty("access_token")
    var accessToken: String = "",
    @JsonProperty("expires_in")
    var expiresIn: Long = 0L,
    @JsonProperty("errcode")
    var errCode: Int? = null,
    @JsonProperty("errmsg")
    var errMsg: String? = null
)