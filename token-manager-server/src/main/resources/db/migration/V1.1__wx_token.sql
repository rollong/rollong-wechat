CREATE SCHEMA "s_wechat";

CREATE TABLE "s_wechat"."apps"
(
    "app_id"                  VARCHAR(31)  NOT NULL,
    "app_secret"              VARCHAR(63)  NOT NULL,
    "app_name"                VARCHAR(255) NULL DEFAULT NULL,
    "type"                    VARCHAR(15)  NOT NULL,
    "access_token"            VARCHAR(255) NULL DEFAULT NULL,
    "access_token_expires_at" TIMESTAMP(3) NULL DEFAULT NULL,
    "enabled"                 BOOLEAN      NOT NULL,
    "note"                    TEXT         NULL DEFAULT NULL,
    "created_at"              TIMESTAMP(3) NOT NULL,
    "updated_at"              TIMESTAMP(3) NOT NULL,
    PRIMARY KEY ("app_id")
);

CREATE TABLE "s_wechat"."wx_ticket"
(
    "app_id"      VARCHAR(31)  NOT NULL,
    "type"        VARCHAR(15)  NOT NULL,
    "ticket"      VARCHAR(255) NOT NULL,
    "expires_at"  TIMESTAMP(3) NOT NULL,
    "created_at"  TIMESTAMP(3) NOT NULL,
    "updated_at"  TIMESTAMP(3) NOT NULL,
    PRIMARY KEY ("app_id", "type")
);